package tim.schoenheider.cs5621.assignment5;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class encapsulating the first of two jobs. The idea behind the job is that
 * a map task reads in nodes, so it builds a graph by listing out the nodes the
 * graph depends on, the nodes that the task has read in, and all of the edges
 * that are known to belong to the graph. Thus, when the list of read nodes
 * isn't the same as the list of nodes in the graph (depends list), the graph is
 * incomplete and is missing edges. The single reducer tries to fix this by
 * reading in all of these graphs, and merging them together when they depend on
 * each others' nodes. Ideally, the reduce task finishes with each dependency
 * list equaling the read list, although when its task ends it simply writes
 * everything.
 * 
 * Thus, the final output for the job is one line per graph, where a line has a
 * dependency list of nodes, a read list of nodes, and then all of the edges
 * (which are bidirectional). Splitting the graph into several disconnected
 * graphs is only possible when the minimum edge weight is large enough, so it
 * is assumed to be. It is also assumed that the reducer has enough memory and
 * hard drive space to work with all of the input data without redundant edges
 * and without small-weighted edges.
 */
public class Job1 extends Configured implements Tool {

    /**
     * A character that is assumed not to appear in the input. It is used to
     * divide sections of output for the Mapper and Reducer.
     */
    public static final char SPLITTER = '\u001E';

    /**
     * Used as the Mapper class for Job1.
     */
    public static class MyMapper extends
    Mapper<LongWritable, Text, NullWritable, Text> {

	// Declare value used to write key/value pairs
	final Text out_value = new Text();

	// Declare a string representing the current node
	String current_node = "";

	// Declare a depend set and edge set for each separate graph we find.
	// and also declare a read set. These are described elsewhere.
	List<Set<String>> depends = new LinkedList<>();
	List<Set<String>> edge_lists = new LinkedList<>();
	Set<String> read = new TreeSet<>();

	/**
	 * For every node, the node is placed in the read list. Then, the Mapper
	 * checks every depend set, and if a depend set contains this current
	 * node, then all of its edges are placed in the corresponding edges
	 * set. If it isn't in any depend set, then it is placed in a new depend
	 * set, and then all of its edges are placed in the corresponding edges
	 * set.
	 * 
	 * All edges with weights less than the minimum are ignored, and all
	 * edges are stored as "nodeA nodeB" in their alphabetical ordering.
	 * Thus, the set can not contain the duplicate edge "nodeB nodeA" since
	 * the order of nodes would be changed to "nodeA nodeB" which would
	 * already be in the set.
	 * 
	 * @param key
	 *            an offset, not used
	 * @param value
	 *            a line of input
	 * @param context
	 *            used to write key/value pairs
	 */
	@Override
	public void map(LongWritable key, Text value, Context context) {

	    double threshold = Double.parseDouble(context.getConfiguration()
		    .get("cliquefinder.threshold"));

	    // Convert the current line to a string
	    String line = value.toString();

	    // Check if this is the first line or not
	    Pattern pat = Pattern.compile("^99999999 [^\\s]+ (.+)$");
	    Matcher m = pat.matcher(line);

	    // Change current node if the file (and thus the node) has changed
	    if (m.matches()) {
		current_node = m.group(1).trim();
		read.add(current_node);

		// Determine if a new dependency list must be made
		boolean make_new = true;
		for (Set<String> depend : depends) {
		    if (depend.contains(current_node)) {
			make_new = false;
		    }
		}

		// Make a new dependency list, no list had this node
		if (make_new) {
		    depends.add(new TreeSet<>());
		    depends.get(depends.size() - 1).add(current_node);

		    edge_lists.add(new TreeSet<>());
		}

		// Quit to advance to next line (definitely an edge!)
		return;
	    }

	    // Check if it is an edge line
	    pat = Pattern.compile("^(\\d+).(\\d+) (.+)$");
	    m = pat.matcher(line);

	    if (m.matches()) {
		// Get the weight and name from the regexp
		double weight = Double.parseDouble(m.group(1) + "."
			+ m.group(2));

		// Ignore edges with small enough weights
		if (weight < threshold) {
		    return;
		}

		// Create an edge to add to our list, make sure nodes are
		// alphabetical to avoid duplicate undirected edges in
		// the set (set doesn't allow duplicates)
		String node = m.group(3);
		String edge;
		if (current_node.compareTo(node) < 0) {
		    edge = current_node + " " + node;
		} else {
		    edge = node + " " + current_node;
		}

		// Check if some list has this node or the connecting node
		for (Set<String> depend : depends) {

		    if (depend.contains(current_node) || depend.contains(node)) {

			// Make sure both nodes are in the dependency list,
			// set won't allow duplicates
			depend.add(current_node);
			depend.add(node);

			// Add this to the list of edges
			int index = depends.indexOf(depend);
			edge_lists.get(index).add(edge);

			// Quit
			return;
		    }
		}

		// The current node is at least in one dependency list!
		throw new CliqueFinder.InitError(current_node + " " + line);
	    }

	    // Neither start of file nor an edge, so throw exception
	    throw new CliqueFinder.InitError(line);
	}

	/**
	 * Called after the Mapper reads in the very last file, necessary to
	 * make sure that context.write() is called.
	 */
	@Override
	public void cleanup(Context context) throws IOException,
	InterruptedException {
	    this.writeData(context);
	}

	/**
	 * Used to write all of the data after the Mapper has read everything
	 * in. It write out the depend set, the read set, and then every
	 * individual edge with each item separated by the splitter character
	 * defined earlier.
	 * 
	 * @param context
	 *            used to write key/value pairs
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void writeData(Context context) throws IOException,
	InterruptedException {
	    for (Set<String> depend : depends) {
		// Get the index of the depend and edge sets
		int index = depends.indexOf(depend);

		// Create a string containing the depend set
		String dependString = "";
		for (String node : depend) {
		    dependString += node + " ";
		}

		// Create a string containing the read set
		String readString = "";
		for (String node : read) {
		    readString += node + " ";
		}

		// Create a string containing all of the edges
		String edgeString = "";
		for (String edge : edge_lists.get(index)) {
		    edgeString += edge + SPLITTER;
		}

		// Merge the strings together, creating a single value
		out_value.set(dependString.trim() + SPLITTER
			+ readString.trim() + SPLITTER + edgeString);

		// Write out the value
		context.write(NullWritable.get(), out_value);
	    }
	}
    }

    /**
     * Used as the Reducer and Combiner class.
     */
    public static class MyReducer extends
    Reducer<NullWritable, Text, NullWritable, Text> {

	// Declare variables used for writing the key/value pair
	Text out_value = new Text();

	// Create a depend set, read set, and edge set for each disconnected
	// graph
	List<Set<String>> depends = new LinkedList<>();
	List<Set<String>> reads = new LinkedList<>();
	List<Set<String>> edges = new LinkedList<>();

	/**
	 * Takes in graphs from the Mapper class that may or may not be
	 * connected by edges. The reduce method stores every graph until either
	 * cleanup occurs or the depend set is equal to the read set. The point
	 * is that the Mapper may not have read everything in a particular
	 * graph, so when two map tasks produce graphs that need to be
	 * connected, this single reducer tries to merge them into a single,
	 * complete graph with all of its nodes and edges.
	 * 
	 * The Reducer takes in a graph consisting of a depend set, read set,
	 * and edge set. If the depend set does not equal the read set, the
	 * graph is stored in a list. New graphs that are read in are either
	 * merged with an existing graph when they depend on each others' nodes,
	 * stored separately when the depend list doesn't match the read list,
	 * or written to context when they are.
	 * 
	 * Since "reducing" is mostly a merge operation, it is used as the
	 * combiner class as well. As the combiner, it needs to write out the
	 * read list despite the fact that Job2 does not need it.
	 */
	@Override
	public void reduce(NullWritable key, Iterable<Text> values,
		Context context) throws IOException, InterruptedException {

	    // Loop through all key/value pairs (since all keys are
	    // NullWritable)
	    valueLoop: for (Text value : values) {

		String[] graph = value.toString().split("" + SPLITTER);

		// Get the depend set for this graph
		Set<String> dependSet = new TreeSet<>();
		for (String node : graph[0].split(" ")) {
		    dependSet.add(node);
		}

		// Get the read set for this graph
		Set<String> readSet = new TreeSet<>();
		for (String node : graph[1].split(" ")) {
		    readSet.add(node);
		}

		// Get the edge set for this graph
		Set<String> edgeSet = new TreeSet<>();
		for (int i = 2; i < graph.length; i++) {
		    edgeSet.add(graph[i]);
		}

		// Write it if we've read everything for it
		if (dependSet.equals(readSet)) {
		    // Write the graph directly to disk, no storing in lists
		    context.write(NullWritable.get(), value);

		    // Quit, move on to next graph (key/value pair)
		    continue valueLoop;
		}

		// Otherwise look to merge...
		for (Set<String> otherDepend : depends) {
		    for (String node : dependSet) {
			if (otherDepend.contains(node)) {
			    int index = depends.indexOf(otherDepend);

			    // Merge the depend lists
			    otherDepend.addAll(dependSet);

			    // Merge the read lists
			    reads.get(index).addAll(readSet);

			    // Merge the edges
			    edges.get(index).addAll(edgeSet);

			    // Write the graph if we've read all its nodes
			    if (otherDepend.equals(reads.get(index))) {
				this.writeGraph(context, index);
			    }

			    // Quit, move on to next graph (key/value pair)
			    continue valueLoop;
			}
		    }

		}

		// Otherwise add to the data structure ...
		depends.add(dependSet);
		reads.add(readSet);
		edges.add(edgeSet);
		continue valueLoop;
	    }
	}

	/**
	 * Writes out the graph defined by the depend set, read set, and edge
	 * set at the specified index in the three lists.
	 * 
	 * @param context
	 *            used to write key/value pairs
	 * @param index
	 *            used to know where in the three lists the graph is defined
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void writeGraph(Context context, int index) throws IOException,
	InterruptedException {

	    // Create a string containing the depend set
	    String dependString = "";
	    for (String node : depends.get(index)) {
		dependString += node + " ";
	    }

	    // Create a string containing the read set
	    String readString = "";
	    for (String node : reads.get(index)) {
		readString += node + " ";
	    }

	    // Create a string containing the edge set
	    String edgeString = "";
	    for (String edge : edges.get(index)) {
		edgeString += edge + SPLITTER;
	    }

	    // Merge the strings together, creating a single value
	    out_value.set(dependString + SPLITTER + readString + SPLITTER
		    + edgeString);

	    // Write the graph to disk
	    context.write(NullWritable.get(), out_value);

	    // Remove it from memory
	    depends.remove(index);
	    reads.remove(index);
	    edges.remove(index);
	}

	/**
	 * Ran when the reduce task is ending. Writes all graphs in the data
	 * structures to context, regardless of whether they are complete or
	 * not. This allows the program to account for imperfect data such as
	 * not having a file for every node.
	 */
	@Override
	public void cleanup(Context context) throws IOException,
	InterruptedException {
	    // Write everything
	    while (!depends.isEmpty()) {
		this.writeGraph(context, 0);
	    }
	}
    }

    /**
     * Used by ToolRunner to run the first job.
     * 
     * @param args
     *            the parameters, contains the input directory and the output
     *            directory
     * @throws Exception
     */
    @Override
    public int run(final String[] args) throws Exception {
	// Get the arguments
	final String[] otherArgs = new GenericOptionsParser(this.getConf(), args)
	.getRemainingArgs();

	// Configure the job
	final JobConf conf = new JobConf(this.getConf());
	conf.setJobName("job 1");

	// Pass variables to Mapper and Reducer
	conf.setInt("cliquefinder.min", Integer.parseInt(otherArgs[2]));
	conf.setStrings("cliquefinder.threshold", otherArgs[3]);

	// Set options as described in CliqueFinder.java
	conf.set("mapred.child.java.opts", CliqueFinder.JAVA_OPTS);
	conf.set("mapred.job.map.memory.mb", CliqueFinder.MAP_MEMORY);
	conf.set("mapred.job.reduce.memory.mb", CliqueFinder.REDUCE_MEMORY);
	conf.set("mapred.task.timeout", CliqueFinder.TIMEOUT);
	conf.set("io.sort.mb", CliqueFinder.BUFFER);

	// Compress intermediate Mapper output, minimizes disk usage
	conf.set("mapred.compress.map.output", "true");
	conf.set("mapred.output.compression.type", "BLOCK");
	conf.set("mapred.map.output.compression.codec",
		"org.apache.hadoop.io.compress.BZip2Codec");
	conf.setCompressMapOutput(true);

	// Enable jvm reuse, map tasks are short-lived
	conf.setNumTasksToExecutePerJvm(-1);

	// Setup the job
	final Job job = new Job(conf);
	job.setMapperClass(MyMapper.class);
	job.setCombinerClass(MyReducer.class);
	job.setReducerClass(MyReducer.class);
	job.setJarByClass(CliqueFinder.class);

	// Use 1 reducer to merge the graphs together
	job.setNumReduceTasks(1);

	job.setMapOutputKeyClass(NullWritable.class);
	job.setMapOutputValueClass(Text.class);

	job.setOutputKeyClass(NullWritable.class);
	job.setOutputValueClass(Text.class);

	FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
	// FileOutputFormat.setCompressOutput(job, true);

	// Submit job for completion
	return job.waitForCompletion(true) ? 0 : 1;
    }
}
