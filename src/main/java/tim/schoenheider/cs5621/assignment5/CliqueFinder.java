package tim.schoenheider.cs5621.assignment5;

import org.apache.hadoop.util.ToolRunner;

/**
 * @author Tim Schoenheider
 * 8 May 2013
 * CS 5621 - Architecture
 * Professor Ted Pedersen
 * Programming Assignment #5
 * 
 * **************************************************************************
 *  1) Problem
 *  The problem is to write a Hadoop program that will find all cliques of a
 *  given size (and above) that exist in a very large graph. In that way,
 *  the program is to take in individual files representing nodes and their
 *  adjacency lists in order to find all cliques in the overall graph that
 *  involve enough nodes connecting via edges of a certain weight and larger.
 * 
 * **************************************************************************
 *  2) Input, output, and usage
 * 	Input:
 * 		The input folder to this program must only contain text files
 * 		where each file represents a node and its adjacency list. In
 * 		these files, it is assumed that the first line matches:
 * 			^99999999 [^\\s]+ (.+)$
 * 		where (.+) is the name of the node. All other lines match:
 * 			^(\\d+).(\\d+) (.+)$
 * 		where (\\d+).(\\d+) make up the weight and (.+) is the name
 * 		of the node that this edge connects to. The following is an
 * 		example of the first line declaring a node followed by two edges:
 * 			99999999 WordNet::Similarity::wup 0#n#1
			1.000000 0#n#1
			0.777778 1000000000000#n#1
 * 	Output:
 * 		The output has a clique on every line, and consists of the names
 * 		of nodes separated by spaces. They are in no particular order,
 * 		are a maximum clique that cannot be expanded, and contain no
 * 		duplicate entries. The following line is an example of a line
 * 		of output which shows a maximum clique between three nodes:
 * 			nodeB nodeD nodeA
 * 	Usage:
 * 		The program takes 5 parameters. The first is the input
 * 		directory, the second is the output directory, the third is the
 * 		minimum clique size (by # of nodes) for a clique to be reported,
 * 		the fourth is a threshold that filters out all edges less than
 * 		the parameter, and the fifth parameter is an empty, temporary
 * 		directory. So an example of running the program would be:
 * 			hadoop jar CliqueFinder.jar tim.CliqueFinder input/
 * 				output/ 4 0.95 temp/

 * **************************************************************************
 *  3) Solution / Algorithm
 * 
 *  The program is split into 2 jobs. All code is contained in three files.
 *  CliqueFinder.java contains some an Error class, several job configuration
 *  parameters shared among the two jobs, and a main method that runs jobs.
 *  Job1.java contains the entirety of the first job, while Job2.java contains
 *  the second and final job.
 * 
 *  (Brief summary)
 *  The flow of execution is as follows:
 *  	CliqueFinder's main begins submitting jobs, waiting for each job, and
 *  	feeding each job's output into the next jobs input.
 * 
 *  	Job 1: Produces lines of output representing separate, disjoint graphs.
 * 
 *  	Job 2: Produces the cliques found in each separate graph.
 * 
 * **Assumptions**************************************************************
 * 
 * It is assumed that all edges from a given node are bidirectional and that
 * the other node may or may not exist. In that way, the program can handle
 * missing node's and edges, while ignoring duplicate edges such as nodeA to
 * nodeB versus nodeB to nodeA.
 * 
 * It is also assumed that the "min" and "threshold" parameters described in
 * Usage are large enough to split the entire input into separate, disconnected
 * graphs. In that way the program depends on these parameters being large
 * enough to consider the input as a set of disconnected graphs. Otherwise
 * execution time will be too large when working with an extremely large
 * graph as input.
 * 
 * It is also assumed that the character Job1.SPLITTER does not occur in
 * the input, so that it can be used to divide out sections of output inside
 * of Job1.
 * 
 ***First Job *****************************************************************
 * 
 * In the first job, graphs are defined by a depend set (a list of nodes
 * in the graph), an edges set (a list of all the bidirectional edges of the
 * graph), and a read set (a list of all the node's who's edges are included in
 * the edge set). A graph is considered incomplete if the graph is missing edges
 * for nodes that it depends on, or in other words when the read set is
 * different than the depend set. Considering the data as multiple, separate,
 * disconnected graphs is only possible when enough edges are thrown out as
 * determined by the minimum weight in the parameters.
 * 
 * The Mapper reads in every node and adds it to that Map tasks read set. The
 * mapper stores all its depend sets, read sets, and edge sets for each graph
 * in memory, and writes everything when finished. It reads in each node, checks
 * whether it should add its edges to an existing graph or add them to a new
 * graph. It then adds all of these edges to that graph, updating the depend
 * set for that graph with all of the nodes that the graph depends on. Once
 * cleanup is called, the Mapper finally writes all of these data structures to
 * context, with a key of NullWritable and a value consisting of the graph's
 * three sets.
 * 
 * Of course, the Mapper's view of the world is incorrect so it creates too
 * many graphs. The Reducer and the Combiner are the same, and are merge
 * operations for these graphs. The final Reduce phase involves a single reducer
 * since the merging the separate graphs requires all of the data.
 * 
 * The Reducer takes in these graphs, and stores every graph until that graphs
 * read set is identical to its depend set. It mergers graphs who's depend
 * sets share nodes. In the end, the reducer outputs one graph per line,
 * with a key of NullWritable and value being the string representation of the
 * depend set, read set, and edge set.
 * 
 ***Second Job ****************************************************************
 * In the second job, the Mapper reads in these single-line graphs and writes
 * the graph as the key, with NullWritable as the value. There is no combiner.
 * 
 * Each reducer takes in a graph and instantiates a Vertex defined by the name
 * of each node in the depend set. The Vertex class is defined in Job2. Each
 * vertex has a neighborhood of sets that connect to this vertex. In that way,
 * the set of edges is used to add all connecting nodes to each vertex's
 * neighborhood. Then the Bron-Kerbosch algorithm is run to find all of the
 * cliques in each graph. One clique is written per line.
 * 
 ***Results ****************************************************************
 *
 * While the code was tested locally and appeared to run fine, the program did
 * not successfully run on Itasca. The pbs job began execution at 13:54, at
 * which point data was loaded into HDFS. Later at 19:07, the data was finished
 * being put into HDFS and the program began execution. The generic.e file shows
 * that the program hung at "13/05/08 21:27:42 INFO mapred.JobClient:  map 54% 
 * reduce 18%" in the first mapper. Itasca killed the job at 13:54 for
 * exceeding the wall-clock time of 24 hours at 13:54 the next day. Thus it 
 * appears that some map tasks finished successfully reading in the input,
 * and some combiner tasks were ran. The hung map tasks from job 1 did
 * not make any progress since 21:27.
 *  
 * Taking a look at the log files in the hadoop directory found in $HOME,
 * everything runs smoothly up until the following lines:
 * 2013-05-08 21:43:59,141 INFO org.apache.hadoop.mapred.JobTracker: Lost 
 * 	tracker 'tracker_node0537.localdomain:localhost/127.0.0.1:46639'
 * 2013-05-08 21:43:59,141 INFO org.apache.hadoop.mapred.TaskInProgress: Error
 *  	from attempt_201305081355_0001_m_000011_0: Lost task tracker: 
 *  	tracker_node0537.localdomain:localhost/127.0.0.1:46639
 * 
 * There is only one occurrence of the first line, and a few hundred occurrences
 * of the last line all in a row. This means the map tasks began failing at
 * 21:43, immediately after generic.e's progress report of map 54% and reduce 
 * 18%. In that way, it appears that TaskTracker's JVM died for several map
 * tasks all between 21:43 and 21:47. Reading into more log files, it appears
 * that these tasks died because the JobTracker JVM ran out of memory. Below is
 * the complete contents of another log file:
 * 
 * Exception in thread "IPC Server handler 2 on 54311" java.lang.OutOfMemoryError: Java heap space
Exception in thread "IPC Client (47) connection to node0535/10.1.3.27:51000 from cs562156" java.lang.OutOfMemoryError: Java heap space
	at java.nio.ByteBuffer.allocateDirect(ByteBuffer.java:288)
	at sun.nio.ch.Util.getTemporaryDirectBuffer(Util.java:155)
	at sun.nio.ch.IOUtil.read(IOUtil.java:174)
	at sun.nio.ch.SocketChannelImpl.read(SocketChannelImpl.java:243)
	at org.apache.hadoop.net.SocketInputStream$Reader.performIO(SocketInputStream.java:55)
	at org.apache.hadoop.net.SocketIOWithTimeout.doIO(SocketIOWithTimeout.java:142)
	at org.apache.hadoop.net.SocketInputStream.read(SocketInputStream.java:155)
	at org.apache.hadoop.net.SocketInputStream.read(SocketInputStream.java:128)
	at java.io.FilterInputStream.read(FilterInputStream.java:116)
	at org.apache.hadoop.ipc.Client$Connection$PingInputStream.read(Client.java:342)
	at java.io.BufferedInputStream.fill(BufferedInputStream.java:218)
	at java.io.BufferedInputStream.read(BufferedInputStream.java:237)
	at java.io.DataInputStream.readInt(DataInputStream.java:370)
	at org.apache.hadoop.ipc.Client$Connection.receiveResponse(Client.java:804)
	at org.apache.hadoop.ipc.Client$Connection.run(Client.java:749)
Exception in thread "pool-2-thread-1" java.lang.OutOfMemoryError: Java heap space

 * For that reason, it appears that the JobTracker died at 21:34, when this log
 * file was written. In conclusion, I believe that my program failed because the
 * JobTracker was unable to manage all of the map tasks for the 500GB graph
 * because information about the tasks did not fit in memory. If I had time to
 * run my program again, I would try to increase the JobTracker memory by
 * increasing the initial heap size to 1GB, and the maximum heap size to 2GB
 * through the HADOOP_JOBTRACKER_OPTS environment variable. That would increase
 * its memory by at least an order of magnitude, and allow the JobTracker to
 * continue past the 54% map progress. 
 * 
 * Otherwise it is possible to run separate MapReduce jobs on each subdirectory 
 * in the local input folder, filtering out unneeded edges. The output from 
 * those jobs could then be used as input for this program. Then this code 
 * should be able to run to completion as well, since there would be less input 
 * splits and thus less map tasks.
 */
public class CliqueFinder {

    /**
     * Private constructor, WikiCount object never exists.
     */
    private CliqueFinder() {
    }

    /**
     * An error that is thrown when a variable is not initialized. Used to more
     * easily throw errors and print lines where parsing fails.
     */
    @SuppressWarnings("serial")
    public static class InitError extends Error {

	/**
	 * Private constructor, must use the other constructor.
	 */
	@SuppressWarnings("unused")
	private InitError() {
	}

	/**
	 * Prints that an error has occurred in addition to a message, typically
	 * a line of input where parsing has failed.
	 * 
	 * @param msg
	 *            the message to print
	 */
	public InitError(final String msg) {
	    super("Error: variable not initialized\n\t" + msg);
	}
    }

    /**
     * Set JVM to use a max heap size of 2 GB and an initial heap size of 1024
     * MB
     */
    public static final String JAVA_OPTS = "-Xms1024m -Xmx2g";

    /**
     * Set the memory to allocate for each Map task to 2024 MB.
     */
    public static final String MAP_MEMORY = "2024";

    /**
     * Set the memory to allocate for each Reduce task to 2024 MB.
     */
    public static final String REDUCE_MEMORY = "2024";

    /**
     * Set the memory for the Map tasks buffer's to be 1024 MB. The default is
     * 100 MB, and increasing this value reduces the number of spills by the
     * mappers, by retaining more output in memory before spilling to disk.
     */
    public static final String BUFFER = "512";

    /**
     * Set the time it takes for the JobTracker to wait on a node to be 10
     * minutes, as opposed to the default 1 hour.
     */
    public static final String TIMEOUT = "600000";

    /**
     * Responsible for setting up each job and running them in order.
     * 
     * @param args
     *            an array of length 5 containing the input folder, a temporary
     *            output folder, the final output folder, the m variable, and
     *            the n variable, in that order.
     * @throws Exception
     *             propagates errors and crashes the JVM if something terrible
     *             occurs
     */
    public static void main(final String[] args) throws Exception {
	if (args.length != 5) {
	    System.err.println("Usage: cliquefinder <in> <out> "
		    + "<minNodeSize> <threshold> <temp>");
	}

	// Setup first job arguments
	String[] args1 = new String[4];
	args1[0] = args[0]; // <in>
	args1[1] = args[4]; // <temp>
	args1[2] = args[2]; // <minNodeSize>
	args1[3] = args[3]; // <threshhold>

	// Setup second job arguments
	String[] args2 = new String[4];
	args2[0] = args[4]; // <in>
	args2[1] = args[1]; // <out>
	args2[2] = args[2]; // <minNodeSize>
	args2[3] = args[3]; // <threshhold>

	// Run all the jobs
	final int exitCode1 = ToolRunner.run(new Job1(), args1);
	final int exitCode2 = ToolRunner.run(new Job2(), args2);
    }
}
