package tim.schoenheider.cs5621.assignment5;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.*;

/**
 * A class encapsulating the second of two jobs. The Mapper reads in a graph and
 * writes the graph as the key and NullWritable as the value. The Reducer takes
 * in a disconnected graph and instantiates Vertices with the depend set, and
 * neighborhoods for each vertex using the edges. Then the Bron-kerbosch
 * algorithm is used to find all cliques in the graph.
 */
public class Job2 extends Configured implements Tool {

    /**
     * Used as the Mapper class.
     */
    public static class MyMapper extends
    Mapper<LongWritable, Text, Text, NullWritable> {

	/**
	 * Does nothing except writes the key/value pairs it receives with the
	 * new key being the old value, and NullWritable being the new value.
	 * 
	 * @param key
	 *            an offset, not used
	 * @param value
	 *            a line of text containing a graph
	 * @param context
	 *            used for writing key/value pairs
	 */
	@Override
	public void map(LongWritable key, Text value, Context context)
		throws IOException, InterruptedException {
	    context.write(value, NullWritable.get());
	}

    }

    /**
     * Used as the Reducer class for Job2.
     */
    public static class MyReducer extends
    Reducer<Text, NullWritable, Text, NullWritable> {

	// Declare variables used for writing the key/value pair
	Text out_key = new Text();

	/**
	 * Given a graph defined by a depend set and edge set, the reducer
	 * creates a set of vertices, V(G), for graph G, and a set of edges for
	 * graph G. The Vertex class defined later in Job2 is used for both
	 * vertices and edges. Creating an edge in the reducer means adding
	 * another vertex to one vertex's neighborhood, and vice versa since
	 * edges are bidirectional. The Vertex class stores a vertex's
	 * neighborhood. Then the Bron-Kerbosch algorithm is instantiated on
	 * graph G by feeding it V(G). It finds all the cliques and writes them
	 * to context described later.
	 */
	@Override
	public void reduce(Text key, Iterable<NullWritable> values,
		Context context) throws IOException, InterruptedException {

	    // Initialize min variable
	    int min = Integer.parseInt(context.getConfiguration().get(
		    "cliquefinder.min"));
	    String[] graph = key.toString().split("" + Job1.SPLITTER);

	    for (@SuppressWarnings("unused")
	    NullWritable value : values) {

		Set<Vertex> vertices = new TreeSet<>();
		Map<String, Vertex> map = new LinkedHashMap<>();

		// Create a vertex for each node in the dependency list
		for (String node : graph[0].split(" ")) {
		    Vertex vertex = new Vertex(node.trim());
		    vertices.add(vertex);
		    map.put(node.trim(), vertex);
		}

		// Create an adjacency list for each vertex
		for (int i = 2; i < graph.length; i++) {
		    String edge_string = graph[i];

		    String node1 = edge_string.split(" ")[0].trim();
		    String node2 = edge_string.split(" ")[1].trim();

		    // Find source and target by their String name
		    Vertex source = map.get(node1);
		    Vertex target = map.get(node2);

		    // Skip loopback edges (ABSOLUTELY NECESSARY OR ALGORITHM
		    // FAILS!!!)
		    if (source.equals(target)) {
			continue;
		    }

		    // Edges bidirectional, add both edges to each other's
		    // neighborhood
		    source.addToNeighborhood(target);
		    target.addToNeighborhood(source);
		}

		// Map may be large, let garbage collection kill it
		map.clear();
		map = null;

		// Run algorithm
		this.bronKerbosch3(context, min, vertices);
	    }
	}

	/**
	 * Writes a given clique to context, so that every line of output
	 * consists of each node involved in the clique.
	 * 
	 * @param context
	 * @param min
	 * @param clique
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void printClique(Context context, int min, Set<Vertex> clique)
		throws IOException, InterruptedException {
	    // Build a string representation of the clique
	    String cliqueString = "";
	    for (Vertex v : clique) {
		cliqueString += v.toString() + " ";
	    }

	    // Write out the clique
	    out_key.set(cliqueString);
	    context.write(out_key, NullWritable.get());
	}

	/**
	 * Implementation of Bron-Kerbosch algorithm version 2 as described on
	 * https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm Code is
	 * made to look as close as possible to pseudocode described on
	 * Wikipedia. To instantiate the algorithm, set R = X = { } and P = V(G]
	 * then the algorithm will find all of the cliques in graph G.
	 * 
	 * @param context
	 *            used to write key/value pairs (cliques!)
	 * @param min
	 *            used to filter cliques that are less than this size
	 * @param R
	 *            a set needed by the algorithm
	 * @param P
	 *            a set needed by the algorithm
	 * @param X
	 *            a set needed by the algorithm
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void bronKerbosch2(Context context, int min, Set<Vertex> R,
		Set<Vertex> P, Set<Vertex> X) throws IOException,
		InterruptedException {

	    if (P.isEmpty() && X.isEmpty()) {
		if (R.size() >= min) {
		    // R is a max clique, print it out
		    this.printClique(context, min, R);
		}
	    } else {
		// Choose a pivot vertex u in P u X
		Vertex u = Union(P, X).iterator().next();

		// for each vertex v in P \ N(u)
		for (Iterator<Vertex> i = Complement(P, N(u)).iterator(); i
			.hasNext();) {
		    Vertex v = i.next();

		    // BronKerbosch2(R u {v}, P n N(v), X n N(v))
		    this.bronKerbosch2(context, min, Union(R, v),
			    Intersect(P, N(v)), Intersect(X, N(v)));

		    X.add(v); // X := X u {v}
		    P.remove(v); // P := P \ {v}
		    i.remove(); // P \ {u} = P \ ( {u} u {v}
		}
	    }
	}

	/**
	 * Implementation of Bron-Kerbosch algorithm version 3 as described on
	 * https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm Code is
	 * made to look as close as possible to pseudocode described on
	 * Wikipedia. To instantiate the algorithm, use vertices of the graph as
	 * its input.
	 * 
	 * @param context
	 *            used to write key/value pairs (cliques!)
	 * @param min
	 *            used to filter cliques that are less than this size
	 * @param R
	 *            a set needed by the algorithm
	 * @param P
	 *            a set needed by the algorithm
	 * @param X
	 *            a set needed by the algorithm
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void bronKerbosch3(Context context, int min, Set<Vertex> vertices)
		throws IOException, InterruptedException {
	    // Let P be V(G], and X = R = { }
	    Set<Vertex> P = vertices;
	    Set<Vertex> X = new TreeSet<>();
	    Set<Vertex> R = new TreeSet<>();

	    // Create a degeneracy ordering of the graph G
	    PriorityQueue<Vertex> ordering = new PriorityQueue<>(999,
				(o1, o2) -> {
					if (N(o1).size() < N(o2).size()) {
						return -1;
					} else if (N(o1).size() > N(o2).size()) {
						return 1;
					}
					return 0;
				});

	    // Order all the vertices by their degree, so that small degrees are
	    // first
	    ordering.addAll(vertices);

	    // Proceed with rest of algorithm described by Wikipedia
	    for (Vertex v : ordering) {
		this.bronKerbosch2(context, min, Union(R, v), Intersect(P, N(v)),
			Intersect(X, N(v)));
		P.remove(v); // P := P \ {v}
		X.add(v); // X := X u {v}
	    }

	}

	/**
	 * Returns the neighborhood of Vertex v.
	 * 
	 * @param v
	 * @return
	 */
	public static Set<Vertex> N(Vertex v) {
	    return v.getNeighborhood();
	}

	/**
	 * Returns a new set containing the union of sets s1 and s2.
	 * 
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static Set<Vertex> Union(Set<Vertex> s1, Set<Vertex> s2) {
	    Set<Vertex> set = new HashSet<>();
	    set.addAll(s1);
	    set.addAll(s2);
	    return set;
	}

	/**
	 * Returns a new set containing the intersection of sets s1 and s2.
	 * 
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static Set<Vertex> Intersect(Set<Vertex> s1, Set<Vertex> s2) {
	    Set<Vertex> set = new HashSet<>();
	    set.addAll(s1);
	    set.retainAll(s2);
	    return set;
	}

	/**
	 * Returns a new set containing the union of sets s1 and {v1}.
	 * 
	 * @param s1
	 * @param v1
	 * @return
	 */
	public static Set<Vertex> Union(Set<Vertex> s1, Vertex v1) {
	    Set<Vertex> set = new HashSet<>();
	    set.addAll(s1);
	    set.add(v1);
	    return set;
	}

	/**
	 * Returns a new set containing the complement of s2 in s1. Also written
	 * as s1 \ s2.
	 * 
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static Set<Vertex> Complement(Set<Vertex> s1,
										 Set<Vertex> s2) {
	    Set<Vertex> set = new HashSet<>();
	    set.addAll(s1);
	    set.removeAll(s2);
	    return set;
	}

	/**
	 * Used to store a Vertex and its neighborhood. Needs to implement
	 * Comparable in order to store a Vertex in a TreeSet.
	 */
	public static class Vertex implements Comparable<Vertex> {
	    // Declare name and neighborhood for this vertex
	    private final String name;
	    private final Set<Vertex> neighborhood = new TreeSet<>();

	    /**
	     * Constructor. Vertex is defined by its name, and only that.
	     * 
	     * @param name
	     */
	    public Vertex(String name) {
		this.name = name;
	    }

	    /**
	     * Add another vertex to the neighborhood of this vertex.
	     * 
	     * @param v
	     */
	    public void addToNeighborhood(Vertex v) {
		neighborhood.add(v);
	    }

	    /**
	     * Returns the neighborhood of this vertex. Same as all the nodes
	     * that this vertex shares an edge with.
	     * 
	     * @return the set of all neighbors of V
	     */
	    public Set<Vertex> getNeighborhood() {
		Set<Vertex> duplicate = new TreeSet<>();
		duplicate.addAll(neighborhood);
		return duplicate;
	    }

	    /**
	     * Returns the name of the vertex.
	     */
	    @Override
	    public String toString() {
		return name;
	    }

	    /**
	     * Compares this vertex to the name of another vertex. Needed by
	     * TreeSet class to properly sort its member elements.
	     */
	    @Override
	    public int compareTo(Vertex arg0) {
		return name.compareTo(arg0.toString());
	    }

	    /**
	     * Returns whether this vertex has the same name as another. Needed
	     * by TreeSet class to properly sort its member elements.
	     */
	    @Override
	    public boolean equals(Object other) {
		// Don't compare other objects
		if (other == null || other.getClass() != this.getClass()) {
		    return false;
		}

		// Compare by name
		return name.equals(other.toString());
	    }
	}
    }

    @Override
    public int run(String[] args) throws Exception {
	// Get the arguments
	final String[] otherArgs = new GenericOptionsParser(this.getConf(), args)
	.getRemainingArgs();

	// Configure the job
	final JobConf conf = new JobConf(this.getConf());
	conf.setJobName("job 2");

	// Pass variables to Mapper and Reducer
	conf.setInt("cliquefinder.min", Integer.parseInt(otherArgs[2]));
	conf.setStrings("cliquefinder.threshold", otherArgs[3]);

	// Set options as described in CliqueFinder.java
	conf.set("mapred.child.java.opts", CliqueFinder.JAVA_OPTS);
	conf.set("mapred.job.map.memory.mb", CliqueFinder.MAP_MEMORY);
	conf.set("mapred.job.reduce.memory.mb", CliqueFinder.REDUCE_MEMORY);
	conf.set("mapred.task.timeout", CliqueFinder.TIMEOUT);
	conf.set("io.sort.mb", CliqueFinder.BUFFER);

	// Compress intermediate Mapper output, minimizes disk usage
	conf.set("mapred.compress.map.output", "true");
	conf.set("mapred.output.compression.type", "BLOCK");
	conf.set("mapred.map.output.compression.codec",
		"org.apache.hadoop.io.compress.BZip2Codec");
	conf.setCompressMapOutput(true);

	// Enable jvm reuse, map tasks are short-lived
	conf.setNumTasksToExecutePerJvm(-1);

	// Setup the job
	final Job job = new Job(conf);
	job.setMapperClass(MyMapper.class);
	job.setReducerClass(MyReducer.class);
	job.setJarByClass(CliqueFinder.class);

	// Use 16 of 18 reducers to find all maximal cliques
	job.setNumReduceTasks(18);

	job.setOutputKeyClass(Text.class);
	job.setOutputValueClass(NullWritable.class);

	FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
	FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
	// FileOutputFormat.setCompressOutput(job, true);

	// Submit job for completion
	return job.waitForCompletion(true) ? 0 : 1;
    }

}
