plugins {
    java
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.apache.hadoop:hadoop-client:2.10.1")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks {
    jar {
        manifest {
            attributes.put("Main-Class", "tim.schoenheider.cs5621.assignment5.CliqueFinder")
        }
    }
}