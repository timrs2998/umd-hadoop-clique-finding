## UMD Hadoop Clique-Finding ##
A Hadoop 1.0.3 job that finds cliques in pre-computed pairwise similarity values for nouns and verbs. Written during CS 5621 Architecture under Dr. Ted Pedersen. Read CliqueFinder.java for more details. You can follow the instructions below to acquire the dependencies and run the code.

### Compiling ###

You will need gradle and git to download and run the project. To ensure that they're installed on Ubuntu, you can run:

    sudo apt-get install gradle git

Then you can acquire the source from either Bitbucket or GitHub:
    
    git clone https://bitbucket.org/timrs2998/umd-hadoop-clique-finding.git

Before compiling, you may want to read CliqueFinder.java and change some of
the JVM options, timeouts, and buffer sizes to prevent runtime errors. Oherwise, you can compile the source as follows:

    cd umd-hadoop-clique-finding/
    gradle clean jar

If you have problems with gradle being unable to find the System Java Compiler, try using a later version (>= 2.0) of gradle.

### Running ###

The job uses pre-computed pairwise similarity values (an extremely large graph!) as its input. You can find more information about this dataset at http://www.d.umn.edu/~tpederse/similarity.html .

Once you've acquired the dataset (or part of it), you can place it in an input folder for the job. Then, with Hadoop in the PATH, you can run the job as follows:

    hadoop jar build/libs/CliqueFinder.jar tim.schoenheider.cs5621.assignment5.CliqueFinder input/ output/ 4 0.95 temp/

The job will then find all cliques with at least 4 nodes connected by edges with a weight of 0.95 or greater. See CliqueFinder.java for an explanation of the output.

### Sources ###

The following were used as references:

 * Hadoop: The Definitive Guide, 3rd Edition by Tom White. ISBN-13: 978-1449311520
 * https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm was used to implement the Bron-Kerbosch algorithm version 2.

### Licensing ###

All code is licensed under the GNU GENERAL PUBLIC LICENSE Version 3.